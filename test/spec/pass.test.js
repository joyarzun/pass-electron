const path = require("path");
const { promisify } = require("util");
const open = promisify(require("fs").open);
const unlink = promisify(require("fs").unlink);
const mkdir = promisify(require("fs").mkdir);
const homedir = require("os").homedir();
const passdir = path.join(homedir, ".password-store");
const mockFileName = "onelargetestthatdonotexist";
const mockFile = path.resolve(passdir, mockFileName);

describe("pass", () => {
  let pass = require("../../src/pass");

  describe("decryptFile", () => {
    const mockString = "test string";

    beforeAll(() => {
      resetAndMockGPG(null, mockString);
    });

    afterAll(() => {
      jest.unmock("gpg");
    });

    it("should resolve with plain password", () =>
      expect(pass.decryptFile(mockFile)).resolves.toBe(mockString));

    describe("Error", () => {
      const mockError = "test error";

      beforeAll(() => {
        resetAndMockGPG(mockError, mockString);
      });

      it("should reject with error", () =>
        expect(pass.decryptFile(mockFile)).rejects.toMatch(mockError));
    });
  });

  describe("passEntries", () => {
    const mockFileGPG = `${mockFile}.gpg`;
    beforeAll(() => {
      return mkdir(passdir).catch(e => e).then(() => open(mockFileGPG, "a"));
    });

    afterAll(() => {
      return unlink(mockFileGPG);
    });

    it("should resolve contain test entry", () =>
      expect(pass.passEntries()).resolves.toContain(mockFileName));
  });

  const resetAndMockGPG = (mockError, mockString) => {
    jest.resetModules();
    jest.mock("gpg", () => ({
      decryptFile: (f, cb) => cb(mockError, Buffer.from(mockString))
    }));
    pass = require("../../src/pass");
  };
});
