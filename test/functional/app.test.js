const Application = require("spectron").Application;
const path = require("path");
const appPath = path.resolve(`${__dirname}/../../node_modules/.bin/electron`);

describe("application launch", () => {
  beforeEach(() => {
    this.app = new Application({
      path: appPath,
      args: [path.join(__dirname, "../..")]
    });
    return this.app.start();
  });

  afterEach(() => {
    if (this.app && this.app.isRunning()) {
      return this.app.stop();
    }
  });

  it("shows an initial window", () => {
    return this.app.client.getWindowCount().then(count => {
      expect(count).toBe(1);
    });
  });

  it("hides when Escape is pressed", () => {
    this.app.browserWindow.show();
    this.app.browserWindow.focus();

    const event = {
      type: "keyDown",
      keyCode: "Escape"
    };
    this.app.webContents.sendInputEvent(event);

    return this.app.browserWindow
      .isVisible()
      .then(visible => expect(visible).toBe(false));
  });
});

describe("search box", () => {
  beforeEach(() => {
    this.app = new Application({
      path: appPath,
      args: [path.join(__dirname, "../..")]
    });
    return this.app.start();
  });

  afterEach(() => {
    if (this.app && this.app.isRunning()) {
      return this.app.stop();
    }
  });

  it("exists an input with id search", () => {
    const element = this.app.client.element("#search");
    const expectedElement = {
      value: expect.anything()
    };
    return expect(element).resolves.toMatchObject(expectedElement);
  });

  it("focus on first element when down key is pressed", done => {
    this.app.client.keys("ArrowDown");

    const elementExpected = this.app.client
      .element("#mountList ul li:nth-child(1)")
      .getText();
    const elementActive = this.app.client.elementActive().getText();
    elementExpected.then(expected => {
      elementActive.then(active => {
        expect(active).toBe(expected);
        done();
      });
    });
  });

  it("focus on search box when up key is pressed in first element", done => {
    this.app.client.keys("ArrowDown");
    this.app.client.keys("ArrowUp");

    const elementExpected = this.app.client.element("#search").getTagName();
    const elementActive = this.app.client.elementActive().getTagName();
    elementExpected.then(expected => {
      elementActive.then(active => {
        expect(active).toBe(expected);
        done();
      });
    });
  });

  it("takes the first one from the list of keys when Enter is pressed on search", () => {
    this.app.browserWindow.show();
    this.app.browserWindow.focus();

    return this.app.client
      .keys("a")
      .keys("Return")
      .then(() =>
        this.app.browserWindow
          .isVisible()
          .then(visible => expect(visible).toBe(false))
      );
  });

  it("selects all text in search when focus", () => {
    const browserWindow = this.app.browserWindow;
    const client = this.app.client;
    const that = this;
    browserWindow.show();
    browserWindow.focus();

    const getElement = () => client.element("#search");

    return getElement()
      .keys("abcd")
      .then(() => browserWindow.hide())
      .then(() => browserWindow.show())
      .then(() => getElement().keys("Backspace"))
      .then(() =>
        getElement()
          .getValue()
          .then(text => expect(text).toBe(""))
      );
  });
});
