const gpg = require("gpg");
const path = require("path");
const glob = require("glob");
const { clipboard } = require("electron");
const homedir = require("os").homedir();
const passdir = path.join(homedir, ".password-store");
const timeToClear = 45000;

const decryptFile = file =>
  new Promise((resolve, reject) => {
    const filepath = path.resolve(passdir, `${file}.gpg`);
    gpg.decryptFile(filepath, (err, content) => {
      if (err) console.log(err);
      if (err) return reject(err);
      return resolve(content.toString("utf8").replace(/\n$/, ""));
    });
  });

const passEntries = () =>
  new Promise((resolve, reject) => {
    const options = { cwd: passdir };

    glob("**/*.gpg", options, (err, files) => {
      if (err) return reject(err);
      const passList = files.map(file => file.replace(/\.gpg$/, ""));
      return resolve(passList);
    });
  });

const decryptFileClipboard = file =>
  decryptFile(file).then(pw => clipboard.writeText(pw)).then(
    setTimeout(() => {
      clipboard.clear();
    }, timeToClear)
  );

module.exports = { decryptFile, passEntries, decryptFileClipboard };
