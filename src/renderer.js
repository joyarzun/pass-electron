const m = require("mithril");
const ignoreCase = require("ignore-case");
const pass = require("./pass");
if (process.env.NODE_ENV == "test") {
  pass.decryptFileClipboard = () => Promise.resolve("hello");
}

const browserWindow = require("electron").remote.getCurrentWindow();
let globalList;

window.addEventListener("focus", () => {
  document.getElementById("search").focus();
  document.getElementById("search").select();
});

window.addEventListener(
  "keydown",
  function(e) {
    // space and arrow keys
    if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
      e.preventDefault();
    }
    if (e.key == "Escape") browserWindow.hide();
  },
  false
);

const mapToListComponent = element =>
  m(
    "li",
    {
      onclick: () => {
        console.log(element);
        pass.decryptFileClipboard(element);
        browserWindow.hide();
      },
      tabindex: 2,
      onkeyup: nav
    },
    element
  );

const nav = e => {
  const li = e.target;
  if (e.key == "ArrowDown") {
    li.nextSibling.focus();
  }
  if (e.key == "ArrowUp") {
    if (li.previousSibling == null) document.getElementById("search").focus();
    else li.previousSibling.focus();
  }
  if (e.key == "Enter") {
    li.click();
  }
};

const doSearch = e => {
  const input = e.target || e.srcElement;
  const text = input.value;
  if (text.length >= 1 && !["ArrowDown", "Meta", "Alt"].includes(e.key)) {
    const result = globalList.filter(element =>
      ignoreCase.includes(element, text)
    );
    renderList(Promise.resolve(result));
  } else if (e.key == 8) {
    renderList(pass.passEntries());
  }
  if (e.key == "ArrowDown") {
    document.getElementById("mountList").firstChild.firstChild.focus();
  }
  if (e.key == "Enter") {
    document.getElementById("mountList").firstChild.firstChild.click();
  }
  console.log(e.key);
};

const searchBox = () =>
  m("input", {
    type: "text",
    placeholder: "search you key",
    onkeyup: doSearch,
    id: "search",
    tabindex: 1
  });

const saveList = list => {
  globalList = list;
  m.mount(document.getElementById("mountInput"), { view: () => searchBox() });
  document.getElementById("search").focus();
  return list;
};

const renderList = (promiseList, save = false) =>
  promiseList
    .then(list => (save ? saveList(list) : list))
    .then(list => list.map(mapToListComponent))
    .then(listComponents => ({
      view: () => m("ul", listComponents)
    }))
    .then(component =>
      m.mount(document.getElementById("mountList"), component)
    );

renderList(pass.passEntries(), true);
